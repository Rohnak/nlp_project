# coding=UTF-8
from __future__ import division
from Tkinter import *
from goose import Goose
from readability.readability import Document
from boilerpipe.extract import Extractor
from pprint import pprint
import google
import urllib,urllib2,requests,re,sys,simplejson, json, sys, urlparse

f = open('tweet_info','r')
data = f.read()
lines = data.splitlines()
proper_nouns = eval(lines[0])
nouns = eval(lines[1])
hashtag = eval(lines[2])[0][0]
urls_ = eval(lines[3])
best_urls = eval(lines[4])
best_url = []
nearby_words = []
for i in range(5,10):
    nearby_words.append(eval(lines[i]))

p_q = ''
c_q = ''
sum = 0
queries = []


for i in range(len(proper_nouns)):
    sum = sum + proper_nouns[i][1]+nouns[i][1]

for i in best_urls:
    best_url.append(i[0])

if len(best_urls)==0:
    for i in urls_:
        best_url.append(i[0])
        
avg = sum/(2*len(proper_nouns))
threshold = avg-(float(avg/2))

for i in range(len(proper_nouns)):
    if nouns[i][1]>threshold:
        c_q = c_q+nouns[i][0]+' '
    if proper_nouns[i][1]>threshold:
        p_q = p_q + proper_nouns[i][0]+' '
        c_q = c_q + proper_nouns[i][0]+' '
    
hashtag = hashtag[1:]


if len(p_q)>0:
    queries.append(p_q)
if len(c_q)>0:
    queries.append(c_q)
if len(hashtag)>0:
    queries.append(hashtag)
if len(best_url[0])>0:
    queries.append(best_url[0])

    
for j in range(len(proper_nouns)):
    query_nearby = proper_nouns[j][0]
    for i in nearby_words[j]:
        if 'BOS' not in i[0] and 'EOS' not in i[0]:
            query_nearby = query_nearby+" "+i[0]
    queries.append(query_nearby)

class SummaryTool(object):

    def split_content_to_sentences(self, content):
        content = content.replace("\n", ". ")
        return content.split(". ")

    def split_content_to_paragraphs(self, content):
        return content.split("\n\n")

    def sentences_intersection(self, sent1, sent2):

        s1 = set(sent1.split(" "))
        s2 = set(sent2.split(" "))

        if (len(s1) + len(s2)) == 0:
            return 0

        return len(s1.intersection(s2)) / ((len(s1) + len(s2)) / 2)

    def format_sentence(self, sentence):
        sentence = re.sub(r'\W+', '', sentence)
        return sentence

    
    def get_senteces_ranks(self, content):

        sentences = self.split_content_to_sentences(content)

        n = len(sentences)
        values = [[0 for x in xrange(n)] for x in xrange(n)]
        for i in range(0, n):
            for j in range(0, n):
                values[i][j] = self.sentences_intersection(sentences[i], sentences[j])

       
        sentences_dic = {}
        for i in range(0, n):
            score = 0
            for j in range(0, n):
                if i == j:
                    continue
                score += values[i][j]
            sentences_dic[self.format_sentence(sentences[i])] = score
        return sentences_dic

    def get_best_sentence(self, paragraph, sentences_dic):

        sentences = self.split_content_to_sentences(paragraph)

        if len(sentences) < 2:
            return ""

        best_sentence = ""
        max_value = 0
        for s in sentences:
            strip_s = self.format_sentence(s)
            if strip_s:
                if sentences_dic[strip_s] > max_value:
                    max_value = sentences_dic[strip_s]
                    best_sentence = s

        return best_sentence

    def get_summary(self, title, content, sentences_dic):

        paragraphs = self.split_content_to_paragraphs(content)

        summary = []
        summary.append(title.strip())
        summary.append("")

        for p in paragraphs:
            sentence = self.get_best_sentence(p, sentences_dic).strip()
            if sentence:
                summary.append(sentence)

        return ("\n").join(summary)

def url_fix(s, charset='utf-8'):
    if isinstance(s, unicode):
        s = s.encode(charset, 'ignore')
    scheme, netloc, path, qs, anchor = urlparse.urlsplit(s)
    path = urllib.quote(path, '/%')
    qs = urllib.quote_plus(qs, ':&=')
    return urlparse.urlunsplit((scheme, netloc, path, qs, anchor))
def main():
    
    for quer in queries:
        
        quer=quer.replace('#','')

        try:
            summary_sum = ""
            news_url = []
            query = quer
            print "query --> ",query
            url = (url_fix('https://ajax.googleapis.com/ajax/services/search/news?' +
               'v=1.0&q='+query+'&userip=INSERT-USER-IP'))
            request = urllib2.Request(url, None, {'Referer': 'www.google.com'})
            response = urllib2.urlopen(request)

            results = simplejson.load(response)
            for i in results["responseData"]["results"]:
                news_url.append(i["unescapedUrl"])
            g_c = 0
            for url in google.search(quer, num=1, stop=1):
                if g_c == 0:
                    gurl = url
                    news_url.append(gurl)
                    g_c = g_c+1
                
            if quer==best_url[0]:
                print "using urls from twitter",urls_
                news_url = urls_
                    
            for url in news_url:
                try:
                    try:
                        g = Goose()
                        article = g.extract(url=str(url))
                    except:
                        pass
                    title = article.title
                    content = article.cleaned_text
               
                    st = SummaryTool()

                    sentences_dic = st.get_senteces_ranks(content)

                    summary = st.get_summary(title, content, sentences_dic)
                    if (url==gurl):
                        print ''
                        print "Summary 1"
                        print ''
                        print summary
                        print '------------------------------------------------------'

                    summary_sum = summary_sum+summary
                except:
                 pass

            st = SummaryTool()
            sentences_dic = st.get_senteces_ranks(summary_sum)
            print ''
            print st.get_summary('Summarized Summary using query '+quer,summary_sum,sentences_dic)
            print '------------------------------------------------------'
            print ''
        except:
            pass

    
        
if __name__ == '__main__':
    main()
