import re,sys,collections, operator, string
from nltk.corpus import stopwords
import codecs

f = codecs.open(sys.argv[1], encoding="utf-8")
#f = open(sys.argv[1],'r')
lines = f.read().splitlines()
proper_nouns = []
proper_nouns_dict = {}
nouns = []
urls = []
hashtags = []
stop = stopwords.words('english')
stop.append('rt')
stop = set(stop)
punctuations = list(string.punctuation)

for line in lines:
	tabsplit = re.split(r'\t+', line)
	tabsplit[0] = 'BOS/BOS BOS '+tabsplit[0]+' EOS EOS/EOS'
	tweetwords = tabsplit[0].split()
	tabsplit[1] = 'BOS1 BOS2 '+tabsplit[1]+' EOS1 EOS2'
	tags = tabsplit[1].split()	
	for j in range(len(tags)):
		if tags[j]=='^':
			proper_nouns.append(tweetwords[j])
			nearbywords = []
			for k in range(-2,3):
				if k != 0:
                                        if tweetwords[j+k].lower() not in stop and tweetwords[j+k] not in punctuations and '@' not in tweetwords[j+k] and '#' not in tweetwords[j+k]:
                                                nearbywords.append(tweetwords[j+k])
			if tweetwords[j] not in proper_nouns_dict:
				proper_nouns_dict[tweetwords[j]] = {}		
			for word in nearbywords:
				if word in proper_nouns_dict[tweetwords[j]]:
					proper_nouns_dict[tweetwords[j]][word] = proper_nouns_dict[tweetwords[j]][word] + 1
				else:
					proper_nouns_dict[tweetwords[j]][word] = 1				
		if tags[j]=='N':
			nouns.append(tweetwords[j])
		if tags[j]=='U':
			if 't.c' not in tweetwords[j] and '\\x' not in tweetwords[j]:
				if len(tweetwords[j])>9:
					urls.append(tweetwords[j])
		if tags[j]=='#':
                        hashtags.append(tweetwords[j])
					
c_proper = collections.Counter(proper_nouns)
c_nouns = collections.Counter(nouns)
c_urls = collections.Counter(urls)
c_hashtags = collections.Counter(hashtags)
common_url = c_urls.most_common(5)

proper_urls = []
for line in lines:
	tabsplit = re.split(r'\t+', line)
	tweetwords = tabsplit[0].split()
	tags = tabsplit[1].split()	
	for j in range(len(tags)):
		if tags[j]=='U':
			for url in common_url:
                                if url[0] in tweetwords[j] and len(tweetwords[j])>len(url[0])+2:
                                        proper_urls.append(tweetwords[j])
        
print c_proper.most_common(5)
print c_nouns.most_common(5)
print c_hashtags.most_common(2)
print c_urls.most_common(5)

c_proper_urls = collections.Counter(proper_urls)
print c_proper_urls.most_common(5)

for key,value in c_proper.most_common(5):
	print sorted(proper_nouns_dict[key].iteritems(), key = operator.itemgetter(1))[-4:]
