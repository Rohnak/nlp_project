import requests, sys, re

q = sys.argv[1]
URL = 'https://www.google.com/search?pz=1&cf=all&ned=us&hl=en&tbm=nws&gl=us&as_q={query}&as_occt=any&as_drrb=b&as_mindate={month}%2F%{from_day}%2F{year}&as_maxdate={month}%2F{to_day}%2F{year}&tbs=cdr%3A1%2Ccd_min%3A3%2F1%2F13%2Ccd_max%3A3%2F2%2F13'
def run(**params):
    response = requests.get(URL.format(**params))
    print "response printing...."
    return response.content

news_page = run(query=q, month=3, from_day=2, to_day=2, year=13)
h3s = re.findall(r'<h3 class="r"><a href="/url\?q=(.*?)>', news_page)
print h3s
