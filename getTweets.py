from TwitterSearch import TwitterSearchOrder, TwitterSearch, TwitterSearchException
import argparse

def retrieveTweets(hashtag):
    try:
        tso = TwitterSearchOrder()
        
        tso.set_keywords([hashtag])
    
        #Authentication tokens required by the Twitter Search API
        ts = TwitterSearch(
                consumer_key = 'jt7DraePc5TMqfSz27DHvAnuV',
                consumer_secret = 'K8QJsCnM0D1iTNnecNbIlKv0CDrAufGgOriAFDO42AHu8t3MPd',
                access_token = '1024742023-ZS5rc6cAU5AdgVdSu5OaKeLeWxtftBTn5vzRycx',
                access_token_secret = 'ZZu1e47t7dKmeHksMF34bzzSofUyhsA8rzIVbJ5qiIVJE'
            )
    
        #iterate over results
        for tweet in ts.search_tweets_iterable(tso):
            print("@{} {}".format(tweet['user']['screen_name'], tweet['text']))
    
    except TwitterSearchException as e: #handle any errors
        print(e)


def main(): 
    
    #handle commandline arguments
    parser = argparse.ArgumentParser()
    parser.add_argument("hashtag", help="The hashtag to be used for retrieving tweets.")
    
    args = parser.parse_args()
    
    retrieveTweets(args.hashtag)
    

if __name__ == '__main__' : main()