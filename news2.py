import urllib2
import simplejson
import json, sys
from pprint import pprint


url = ('https://ajax.googleapis.com/ajax/services/search/news?' +
       'v=1.0&q='+sys.argv[1]+'&userip=INSERT-USER-IP')

request = urllib2.Request(url, None, {'Referer': 'www.google.com'})
response = urllib2.urlopen(request)

# Process the JSON string.
results = simplejson.load(response)
for i in results["responseData"]["results"]:
    pprint(i["unescapedUrl"])
# now have some fun with the results...
